{-# LANGUAGE TemplateHaskell #-}
-- | Template Haskell that splices in the version info gotten from the nix-provided 'basalt-version' script
module Basalt.Version where

import Basalt.Version.TH

-- | String representing basalt's version
-- looks something like @"develop \@ 0000000000000000000000000000000000000000[-dirty]"@
-- if something doesn't quite work, it will be something like @"unknown \@ unknown"@
version :: String
version = $(versionSplice)
