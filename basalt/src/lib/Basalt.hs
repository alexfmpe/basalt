{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}

module Basalt where

import Control.Lens (makePrisms, prism, (<&>))
import Control.Monad.Catch
import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.Fail
import Control.Monad.Log
import Data.Foldable
import Data.List (partition)
import Data.List.NonEmpty (NonEmpty, nonEmpty)
import qualified Data.Map as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as T
import System.Directory
import System.Exit
import System.IO
import System.FilePath
import System.IO.Temp
import qualified System.Process as Proc
import System.Which (staticWhich)

import Obelisk.App (Obelisk(..), ObeliskError(..), runObelisk)
import Obelisk.CliApp
import Obelisk.Command.Thunk (ThunkConfig(..), ThunkData(..), ThunkUpdateConfig(..), readThunk, updateThunkToLatest)


type BasaltCliConfig = CliConfig BasaltError

data Target = NixOS | HomeManager

data Action = Switch | TestBuild | Install | Update (Maybe (NonEmpty FilePath))

data BasaltEnv = BasaltEnv
  { basaltEnvAction :: Action
  , basaltEnvTarget :: Target
  , basaltEnvCliConfig :: CliConfig BasaltError
  , basaltEnvRoot :: Maybe FilePath
  }

data BasaltError =
    BasaltError_ProcessError
    { basaltError_err :: ProcessFailure
    , basaltError_mAnn :: Maybe Text
    }
  | BasaltError_MissingProgram Text  -- Program not on path
  | BasaltError_GitRepo              -- Not a git repo
  | BasaltError_Unstructured Text
  deriving (Show)

makePrisms ''BasaltError

instance AsProcessFailure BasaltError where
  asProcessFailure = _BasaltError_ProcessError . prism (, Nothing) f
    where f = \case
            (pf, Nothing) -> Right pf
            pann@(_, Just _) -> Left pann

type MonadBasalt m =
  ( MonadError BasaltError m
  , MonadReader BasaltEnv m
  , MonadIO m
  , CliLog m
  , HasCliConfig BasaltError m
  , MonadMask m
  , CliThrow BasaltError m
  , MonadFail m
  )

newtype BasaltT m a = BasaltT { unBasaltT :: ReaderT BasaltEnv (CliT BasaltError m) a }
  deriving
    ( Functor, Applicative, Monad, MonadIO, MonadThrow, MonadCatch, MonadMask, MonadFail
    , MonadReader BasaltEnv
    , MonadLog Output
    , MonadError BasaltError
    , HasCliConfig BasaltError
    )


runBasaltT :: MonadIO m => BasaltEnv -> BasaltT m a -> m a
runBasaltT env = runCli (basaltEnvCliConfig env) . (flip runReaderT env) . unBasaltT

execBasalt :: BasaltEnv -> IO ()
execBasalt env = runBasaltT env $ do
  action <- fmap basaltEnvAction ask
  case action of
    TestBuild -> testBuild True
    Switch -> switchConfig
    Install -> install
    Update thunks -> update $ Set.fromList . toList <$> thunks

defaultConfig :: (e -> (Text,Int)) -> IO (CliConfig e)
defaultConfig = newCliConfig Informational False False

-- TODO: Figure out a decent error code scheme
defaultObeliskConfig :: IO (CliConfig ObeliskError)
defaultObeliskConfig = defaultConfig $ \case
  ObeliskError_ProcessError (ProcessFailure p code) ann ->
    ( "Process exited with code " <> T.pack (show code) <> "; " <> reconstructCommand p
      <> maybe "" ("\n" <>) ann
    , 1
    )
  ObeliskError_Unstructured msg -> (msg, 1)

-- TODO: Figure out a decent error code scheme
defaultBasaltConfig :: IO (CliConfig BasaltError)
defaultBasaltConfig = defaultConfig $ \case
  BasaltError_ProcessError pf _      -> (showProcessFailure pf, 1)
  BasaltError_MissingProgram program -> (program <> " is not in your path", 1)
  BasaltError_GitRepo                -> ("Not a git repo", 1)
  BasaltError_Unstructured t         -> ("Uncategorized error: " <> t, 1)
  where
    showProcessFailure (ProcessFailure spec ec) = (tshow spec) <> " failed with code: " <> tshow ec

-- | Implementation of 'basalt switch'
switchConfig :: MonadBasalt m => m ()
switchConfig = withSpinner "Switching configuration" $ gitPush Nothing

update :: MonadBasalt m => Maybe (Set FilePath) -> m ()
update pathWhitelist = do
  currentDir <- liftIO getCurrentDirectory
  isBareRepo currentDir >>= \case
    False -> updateNonBareRepo currentDir pathWhitelist
    True -> do
      withSystemTempDirectory "basalt-update-checkout" $ \tmpDir -> do
        gitClone currentDir tmpDir
        updateNonBareRepo tmpDir pathWhitelist
        gitPush $ Just tmpDir

updateNonBareRepo :: MonadBasalt m => FilePath -> Maybe (Set FilePath) -> m ()
updateNonBareRepo repoPath pathWhitelist = do
  cfg <- liftIO defaultObeliskConfig
  let
    isPackedThunk fp = runObelisk (Obelisk cfg) $ readThunk fp >>= \case
      Right (ThunkData_Packed _ _) -> pure True
      Right ThunkData_Checkout -> pure False
      Left _ -> pure False

    expandDirectory dir = (fmap . fmap) (\subDir -> dir <> "/" <> subDir) (listDirectory dir)
    subDirs dir = filterM doesDirectoryExist =<< expandDirectory dir

  -- Determine which thunks we were asked to update
  specifiedThunks <- case fmap toList pathWhitelist of
    Nothing -> liftIO $ filterM isPackedThunk =<< subDirs repoPath
    Just paths -> do
      invalid <- liftIO $ filterM (fmap not . isPackedThunk) paths
      unless (null invalid) $ throwError $ BasaltError_Unstructured $ "some specified paths are not packed thunks: " <> tshow invalid
      pure $ toList paths

  -- Avoid touching untracked thunks
  untrackedFiles <- (fmap . fmap) T.unpack $ listUntrackedFiles repoPath
  let (uneligible, eligible) = partition (`elem` untrackedFiles) specifiedThunks
  case nonEmpty uneligible of
    Nothing -> pure ()
    Just fp -> liftIO $ do
      putStrLn $ "Ignoring untracked thunks: "
      for_ fp putStrLn

  case nonEmpty eligible of
    Nothing -> liftIO $ putStrLn "Found no thunks to update. Exiting."
    Just ts -> do
      let whenThunksAreDirty x = do
            isDirty <- checkDirty repoPath $ Just ts
            when isDirty x

      -- Bail if any thunk to update has un-committed changes
      whenThunksAreDirty $
        throwError $ BasaltError_Unstructured "refusing to update un-committed thunks"

      for_ eligible $ \t ->
        runObelisk (Obelisk cfg) $ updateThunkToLatest (ThunkUpdateConfig Nothing (ThunkConfig Nothing)) t

      -- Skip commit if updates were all no-op
      whenThunksAreDirty $
        commit repoPath "Basalt: update thunks" ts

-- | Implementation of 'basalt build'
testBuild :: (MonadBasalt m, MonadMask m)
          => Bool -- ^ Whether we are build from a dirty working tree, otherwise a commit
          -> m ()
testBuild dirty = do
  currentDir <- liftIO $ getCurrentDirectory
  case dirty of
    True -> do
      nixpkgsPath <- handleNixpkgThunks $ currentDir </> "nixpkgs"
      withSpinner "Testing configuration" $ dryRun currentDir nixpkgsPath
    False -> do
      withSystemTempDirectory "basalt" $ \tmpDir -> do
        archiveHead currentDir tmpDir
        nixpkgsPath <- handleNixpkgThunks $ tmpDir </> "nixpkgs"
        withSpinner "Testing configuration" $ dryRun tmpDir nixpkgsPath

handleNixpkgThunks :: MonadBasalt m => FilePath -> m Text
handleNixpkgThunks path = do
  let instantiate = $(staticWhich "nix-instantiate")
  let instantiateProc = proc instantiate [ "--eval", path, "-A", "path"
                                         , "--arg", "config", "{}"
                                         , "--arg", "overlays", "[]" ]
  T.stripEnd <$> readProcessAndLogOutput (Debug, Debug) instantiateProc


dryRun :: MonadBasalt m => FilePath -> Text -> m ()
dryRun workingDir nixpkgsPath = do
  target <- basaltEnvTarget <$> ask
  mroot <- basaltEnvRoot <$> ask

  -- Create dummy temp files to pass to Nixpkgs, avoiding the default
  -- impure loading from $HOME.
  withSystemTempFile "config.nix" $ \configFile configHandle -> do
    liftIO $ hPutStr configHandle "{}" >> hFlush configHandle
    withSystemTempFile "overlays.nix" $ \overlaysFile overlaysHandle -> do
      liftIO $ hPutStr overlaysHandle "[]" >> hFlush overlaysHandle

      let dryRunArgs = nixBuildArgs $ toBuildExpr target
          envOverride = Map.delete "NIX_PATH" . Map.insert "NIXPKGS_CONFIG" configFile
          nixBuildArgs expr = [
              "-I", "nixpkgs=" <> (T.unpack nixpkgsPath),
              "-I", "nixpkgs-overlays=" <> overlaysFile
            , "-E", expr
            , "--no-out-link"
            , "--show-trace"
            ] <> (fromMaybe [] $ fmap (\case
              "/" -> []
              root -> ["--store", root]) mroot)
      callProcessAndLogOutput (Informational, Informational) $ setEnvOverride envOverride $
        setCwd (Just workingDir) $ proc $(staticWhich "nix-build") dryRunArgs

toBuildExpr :: Target -> String
toBuildExpr target = case target of
  NixOS -> "(import <nixpkgs/nixos> { configuration = import ./configuration.nix; }).system"
  HomeManager -> "(import ./home-manager/home-manager/home-manager.nix { pkgs=import <nixpkgs> {}; confPath=./home.nix; confAttr=\"\"; })"

-------------------------------------------------------------
--  Utility functions

getSpec :: ProcessSpec -> Proc.CmdSpec
getSpec = Proc.cmdspec . _processSpec_createProcess

overCreateProcess' :: ProcessSpec -> (Proc.CreateProcess -> Proc.CreateProcess) -> ProcessSpec
overCreateProcess' = flip overCreateProcess

tshow :: Show a => a -> Text
tshow = T.pack . show

handleCompletedProc :: MonadBasalt m => Proc.CmdSpec -> ExitCode -> m ()
handleCompletedProc spec ec = case ec of
  ExitSuccess -> pure ()
  ExitFailure c -> throwError $ BasaltError_ProcessError (ProcessFailure spec c) Nothing

-------------------------------------------------------------
--  Git Functions

gitPath :: FilePath
gitPath = $(staticWhich "git")

gitConfig :: MonadBasalt m => FilePath -> Text -> m Text
gitConfig repoPath optionName = do
  fmap T.stripEnd $ readProcessAndLogStderr Notice $ proc gitPath
    [ "-C", repoPath
    , "config", T.unpack optionName
    ]

isBareRepo :: MonadBasalt m => FilePath -> m Bool
isBareRepo repoPath = do
  gitConfig repoPath "core.bare" >>= \case
    "true" -> pure True
    "false" -> pure False
    _ -> throwError BasaltError_GitRepo

gitClone :: MonadBasalt m => FilePath -> FilePath -> m ()
gitClone repoPath directory = do
  callProcessAndLogOutput (Notice, Notice) $ proc gitPath [ "clone", repoPath, directory ]

listUntrackedFiles :: MonadBasalt m => FilePath -> m [Text]
listUntrackedFiles repoPath = do
  let p = proc $(staticWhich "git")
        [ "-C", repoPath
        , "ls-files", "-o", "--directory"
        ]
      sanitize fp = T.pack repoPath <> "/" <> T.dropWhileEnd (== '/') fp
  out <- fmap T.stripEnd $ readProcessAndLogStderr Notice $ p
  pure $ fmap sanitize $ T.lines out

checkDirty :: MonadBasalt m => FilePath -> Maybe (NonEmpty FilePath) -> m Bool
checkDirty repoPath onlyPaths = do
  let p = proc $(staticWhich "git") $
        [ "-C", repoPath
        , "diff", "--quiet", "HEAD"
        ] <> maybe [] toList onlyPaths

  (_, _, _, ph) <- liftIO $ Proc.createProcess $ _processSpec_createProcess p
  waitForProcess ph <&> \case
   ExitSuccess -> False
   ExitFailure _ -> True

commit :: MonadBasalt m => FilePath -> Text -> NonEmpty FilePath -> m ()
commit repoPath msg onlyPaths = do
  let p = proc $(staticWhich "git") $
        [ "-C", repoPath
        , "commit"
        , "-m", T.unpack msg
        , "--only"
        ] <> toList onlyPaths

  (_, _, _, ph) <- liftIO $ Proc.createProcess $ _processSpec_createProcess p
  ec <- waitForProcess ph
  flip handleCompletedProc ec $ getSpec p

-- | git rev-parse HEAD
getHead :: MonadBasalt m => Maybe FilePath -> m Text
getHead path = do
  out <- fmap T.stripEnd $ readProcessAndLogStderr Notice $ setCwd path $ proc $(staticWhich "git") revParse
  if isGitHash out
  then pure out
  else throwError BasaltError_GitRepo
   where
     gitHashLength = 40
     isGitHash :: Text -> Bool
     isGitHash out = T.length out == gitHashLength
     revParse = ["rev-parse", "HEAD"]

gitPush :: MonadBasalt m => Maybe FilePath -> m ()
gitPush path = do
  let pushProc = setCwd path $ proc $(staticWhich "git") $ words "push origin master"
  (_, _, _, ph) <- liftIO $ Proc.createProcess $ _processSpec_createProcess pushProc
  ec <- waitForProcess ph
  flip handleCompletedProc ec $ getSpec pushProc

-- git archive --format=tar "$TO_REV" | tar x -C tmpDir
archiveHead :: MonadBasalt m => FilePath -> FilePath -> m ()
archiveHead from to = do
  gitHead <- getHead $ Just from
  withSpinner ("archiving git hash: " <> gitHead) $ do
    p1@(_, mOut1, _, ph1) <- createProcess $ archiveProc gitHead
    case mOut1 of
      Nothing -> do
        liftIO $ Proc.cleanupProcess p1
        throwError $ BasaltError_Unstructured "stdout handle for git proc does not exist"
      Just out1 -> do
        let tarProc' = tarProc out1
        p2@(_, _, _, ph2) <- createProcess tarProc'
        [_, ec] <- sequence [ waitForProcess ph1, waitForProcess ph2 ]
        mapM_ (liftIO . Proc.cleanupProcess) [ p1, p2 ]
        handleCompletedProc (getSpec tarProc') ec
  where
    archiveProc gitHead = let archiveProc' = proc $(staticWhich "git") ["archive", "--format", "tar", T.unpack gitHead] in
      overCreateProcess' archiveProc' $ \cp -> cp {
        Proc.std_out = Proc.CreatePipe,
        Proc.cwd = Just from
        }
    tarProc inputHandle = overCreateProcess (\cp -> cp { Proc.std_in = Proc.UseHandle inputHandle }) $
        proc $(staticWhich "tar") ["-x", "-C", to]

----------------------------------------------------------------------------
-- TODO:
install :: MonadBasalt m => m ()
install = do
  liftIO $ putStrLn "'basalt install' is not a currently supported command"
