with (import ./dep/obelisk {}).nixpkgs;
writeScript "run-impure-test" ''
  tests='exec(os.environ["testScript"])' ${(import ./test/auto { }).driver}/bin/nixos-test-driver
''
