let
  obelisk = import ./dep/obelisk {};
  obelisk-command-overlay = (obelisk.reflex-platform.hackGet ./dep/obelisk) + "/haskell-overlays/obelisk.nix";

  gitignore = obelisk.nixpkgs.fetchFromGitHub {
    owner = "hercules-ci";
    repo = "gitignore";
    rev = "f9e996052b5af4032fe6150bba4a6fe4f7b9d698";
    sha256 = "0jrh5ghisaqdd0vldbywags20m2cxpkbbk5jjjmwaw0gr8nhsafv";
  };
  gitignoreSource = (import gitignore {}).gitignoreSource;

  basalt-version = let
    refs = obelisk.nixpkgs.lib.sourceByRegex ./.git ["refs.*"];
    parseVersion = obelisk.nixpkgs.writeShellScriptBin "basalt-version" ''
      set -euo pipefail
      HEAD=($(cat "${./.git/HEAD}"))
      if [ "''${HEAD[0]}" = "ref:" ]; then
        REF="''${HEAD[1]##*/}"
        HASH="$(cd "${refs}" && cat "''${HEAD[1]}")"
      else
        REF="HEAD"
        HASH="''${HEAD[0]}"
      fi
      RESULT="$REF @ $HASH"
      echo "$RESULT"
    '';
  in parseVersion;

  # Define GHC compiler override
  pkgs = obelisk.obelisk.override {
    overrides = self: super: with obelisk.nixpkgs; rec {
      obelisk-command = (import obelisk-command-overlay self super).obelisk-command;
      basalt = haskell.lib.overrideCabal (pkgs.callCabal2nix "basalt" (gitignoreSource ./basalt) {}) (o: {
        buildTools = (o.buildTools or []) ++ [ basalt-version ];
        executableToolDepends = (o.executableToolDepends or []) ++ [ git nix nix-prefetch-git ];
      });
    };
  };
in {
  inherit basalt-version;
  inherit pkgs;
  inherit (pkgs) basalt;
}
