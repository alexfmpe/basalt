let pkgs = (import ./.).pkgs;
in with pkgs; shellFor {
  withHoogle = true;
  packages = p: [ p.basalt ];
  nativeBuildInputs = [ cabal-install ghcid hlint ];
}
