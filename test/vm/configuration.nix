{ pkgs, modulesPath, ... }:
{
  # Scaffolding necessary for proper operation of the VM
  imports = [
    (modulesPath + "/virtualisation/qemu-vm.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  # Sensible defaults
  # this VM is intended for manual operation
  virtualisation = {
    memorySize = 8192;
    diskSize = 8192;
    cores = 2;
    useBootLoader = true;
    graphics = false;
    # this is the default these days but, just in case!
    writableStore = true;
    # let the VM nix store state persist
    writableStoreUseTmpfs = false;
  };

  # Make qemu use the nearest serial device (usually the terminal emulator you run it in)
  boot.kernelParams = [
    "console=tty1"
    "console=ttyS0,115200"
  ];

  boot.loader.timeout = 0;

  system.name = "basalt-test";

  services.qemuGuest.enable = true;

  # Add reflex-frp cache
  nix.binaryCaches = [ "https://cache.nixos.org/" "https://nixcache.reflex-frp.org" ];
  nix.binaryCachePublicKeys = [ "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI=" ];

  nix.trustedUsers = [ "root" "@wheel" ];

}
