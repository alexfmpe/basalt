#! /usr/bin/env bash

set -euxo pipefail

WORKING_DIR="$(dirname "$(readlink -f -- "${BASH_SOURCE[0]}")")"
# Pass the working dir to the main script so that it can be mounted in the VM
GIT_REPO="$(git -C "$WORKING_DIR" rev-parse --show-toplevel)"

$(nix-build "$WORKING_DIR/default.nix" -A script --no-out-link) "$GIT_REPO" "${1:-}"
